import cv2
import numpy as np
from collections import Counter
import glob as gb
import json
import sys
import count_edit

def count_rgb(input, output):
	result = []
	number = 0
	img_path = gb.glob(input)
	for path in img_path:
		print (path)
		number += 1
		print (number)
		image = cv2.imread(path)
		height, width = image.shape[:2]
		for x in range(height):
			for y in range(width):
				result.append(list(image[x,y]))

	count = Counter(map(tuple, result))
	print (len(result))
	print (len(count))
	print (type(count))
	with open('temp.txt', 'w') as file:
		file.write(json.dumps(str(count)))
	count_edit.edit('temp.txt', output)

print ('********************************')
print ('Instructon: python rgb.py all/each outpt.txt')
print ('********************************')
if sys.argv[1] == "all":
	argv1 = 'frame*.jpg'
	argv2 = sys.argv[2]
	count_rgb(argv1,argv2)
elif sys.argv[1] == 'each':
	#argv2 = sys.argv[2]
	img_path = gb.glob('frame*.jpg')
	number = 0
	for path in img_path:
		result = []
		print (path)
		number += 1
		print (number)
		image = cv2.imread(path)
		height, width = image.shape[:2]
		for x in range(height):
			for y in range(width):
				result.append(list(image[x,y]))
		count = Counter(map(tuple, result))
		with open('temp.txt', 'w') as file:
			file.write(json.dumps(str(count)))
		count_edit.edit('temp.txt', path.split('.')[0] + '.txt')