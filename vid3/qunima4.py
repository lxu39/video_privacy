import numpy as np
from collections import Counter
import glob as gb
import json
import sys

def readall(fname):
    with open(fname) as f:
        all_lines = f.readlines()
    all_lines = [x.strip().split(' ') for x in all_lines]
    result=[]
    for line in all_lines:
        result.append([int(x.strip(',')) for x in line])
    result.sort()
    return result

file_path = gb.glob('../vid3/final/frame*_final.txt')
body_result = []
nobody_result = []
''' i in range(1,120573):
	print (i)
	for path in file_path:
		frame_final = readall(path)
		currentline = frame_final[i-1]
		body_result.append(currentline[5])
		nobody_result.append(currentline[6])
	max_body = max(body_result)
	max_nobody = max(nobody_result)
	with open('cbody_nobody_final.txt', 'a') as file2:
		file2.write(str(currentline[0]) + ' ' + str(currentline[1]) + ' ' + str(currentline[2]) + ' ' + str(currentline[3]) + ' ' + str(currentline[4]) + ' ' + str(max_body) + ' ' + str(max_nobody) + '\n')
'''
print (len(file_path))
for i in range(1, len(file_path)):
	print (i)
	if i == 1:
		frame_final = readall(file_path[i])
		frame_final_pre = readall(file_path[i-1])
		for i in range(len(frame_final)):
			max_body = max(frame_final[i][6],frame_final_pre[i][6])
			max_nobody = max(frame_final[i][7], frame_final_pre[i][7])
			max_back = max(frame_final[i][8], frame_final_pre[i][8])
			frame_final_pre[i][6] = max_body
			frame_final_pre[i][7] = max_nobody
			frame_final_pre[i][8] = max_back
	else:
		frame_final = readall(file_path[i])
		for i in range(len(frame_final)):
			max_body = max(frame_final[i][6],frame_final_pre[i][6])
			max_nobody = max(frame_final[i][7], frame_final_pre[i][7])
			max_back = max(frame_final[i][8], frame_final_pre[i][8])
			frame_final_pre[i][6] = max_body
			frame_final_pre[i][7] = max_nobody
			frame_final_pre[i][8] = max_back
print ('22222')
with open('cbody_notext_background_final.txt', 'a') as file:
	for i in range(len(frame_final_pre)):
		currentline = frame_final_pre[i]
		file.write(str(currentline[0]) + ' ' + str(currentline[1]) + ' ' + str(currentline[2]) + ' ' + str(currentline[3]) + ' ' + str(currentline[4]) + ' ' + str(currentline[5]) + ' ' + str(currentline[6]) + ' ' + str(currentline[7]) + ' ' + str(currentline[8]) + '\n')