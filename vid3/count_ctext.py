import cv2
import numpy as np
from collections import Counter
import json
import count_edit
import sys
import glob as gb

if sys.argv[1] == "all":
    result = []
    with open("body_rectangle.txt", "r") as filestream:
        with open("temp.txt", "w") as filestreamtwo:
            for line in filestream:
                print (line)
                currentline = line.split(" ")
                image = cv2.imread(currentline[0])
                print (currentline[0])
                for x in range(int(currentline[1]), int(currentline[3])):
                    for y in range(int(currentline[2]), int(currentline[4])):
                        result.append(list(image[y,x]))
            with open('intersection.txt', 'r') as file:
                print ('Start !!!!!!!!!!!!!!!!!!!!!')
                for line in file:
                    print (line)
                    currentline = line.split(' ')
                    image = cv2.imread(currentline[0])
                    for x in range(int(currentline[1]), int(currentline[3])):
                        for y in range(int(currentline[2]), int(currentline[4])):
                            result.remove(list(image[y,x]))
            count = Counter(map(tuple, result))
            filestreamtwo.write(json.dumps(str(count)))
    count_edit.edit('temp.txt', 'cbody.txt')
elif sys.argv[1] == "each":
    img_path = gb.glob('frame*.jpg')
    for path in img_path:
        result = []
        with open("text.txt", "r") as filestream:
            with open("temp.txt", "w") as filestreamtwo:
                for line in filestream:
                    currentline = line.split(" ")
                    if currentline[0] == path:
                        print (path)
                        image = cv2.imread(currentline[0])
                        for x in range(int(currentline[1]), int(currentline[3])):
                            for y in range(int(currentline[2]), int(currentline[4])):
                                result.append(list(image[y,x]))
                '''with open('intersection.txt', 'r') as file:
                    for line in file:
                        currentline = line.split(' ')
                        if currentline[0] == path:
                            image = cv2.imread(currentline[0])
                            for x in range(int(currentline[1]), int(currentline[3])):
                                for y in range(int(currentline[2]), int(currentline[4])):
                                    result.remove(list(image[y,x]))
                        else:
                            continue'''
                count = Counter(map(tuple, result))
                filestreamtwo.write(json.dumps(str(count)))
        count_edit.edit('temp.txt', path.split('.')[0] + '_ctext.txt')
        print ('yes')