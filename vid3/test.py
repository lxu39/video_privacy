# import the necessary packages
from __future__ import print_function
from imutils.object_detection import non_max_suppression
from imutils import paths
import numpy as np
import argparse
import imutils
import cv2
import glob as gb
import json

#first four arguments represent first rectangle, last four represent second rectangle
def isOverlap(xa,ya,xb,yb,xc,yc,xd,yd):
	#return not (xb<xc or xa>xd or ya<yd or yb>yc)
	if xa>xd or xb<xc:
		return False
	if ya>yd or yb<yc:
		return False
	return True


def rectIntersection(xa,ya,xb,yb,xc,yc,xd,yd):
	if not isOverlap(xa,ya,xb,yb,xc,yc,xd,yd):
		return
	min_x = max(xa, xc)
	max_x = min(xb, xd)
	max_y = max(ya, yc)
	min_y = min(yb, yd)
	return [min_x,max_y,max_x,min_y]

#print (rectIntersection(75,0,209,265,262,77,344,241))

# initialize the HOG descriptor/person detector
hog = cv2.HOGDescriptor()
hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
# loop over the image paths
# load the image and resize it to (1) reduce detection time
# and (2) improve detection accuracy
img_path = gb.glob("frame*.jpg")
for path in img_path:
	image = cv2.imread(path)
	image = imutils.resize(image, width=min(400, image.shape[1]))
	orig = image.copy()

	# detect people in the image
	(rects, weights) = hog.detectMultiScale(image, winStride=(4, 4),
		padding=(8, 8), scale=1.05)

	# draw the original bounding boxes
	for (x, y, w, h) in rects:
		cv2.rectangle(orig, (357,73), (398,309), (0, 0, 255), 2)

	# apply non-maxima suppression to the bounding boxes using a
	# fairly large overlap threshold to try to maintain overlapping
	# boxes that are still people
	rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
	pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)

	# draw the final bounding boxes
	for (xA, yA, xB, yB) in pick:
		cv2.rectangle(image, (xA, yA), (xB, yB), (0, 255, 0), 2)
		'''with open('body_rectangle.txt', 'a') as file:
			file.write(path +' ' + str(xA) +' ' + str(yA) +' '+ str(xB) +' '+ str(yB) + '\n')



	# show some information on the number of bounding boxes
	filename = "pic"
	print("[INFO] {}: {} original boxes, {} after suppression".format(
		filename, len(rects), len(pick)))


	print (pick)
	print (len(pick))
	if len(pick) > 1:
		for i in range(len(pick)):
			for j in range(i+1,len(pick)):
				temp = []
				temp.append([pick[i],pick[j]])
				#print (temp)
				if isOverlap(temp[0][0][0],temp[0][0][1],temp[0][0][2],temp[0][0][3],temp[0][1][0],temp[0][1][1],temp[0][1][2],temp[0][1][3]):
					with open('intersection.txt', 'a') as file:
						inter_list = rectIntersection(temp[0][0][0],temp[0][0][1],temp[0][0][2],temp[0][0][3],temp[0][1][0],temp[0][1][1],temp[0][1][2],temp[0][1][3])
						file.write(path + ' ' + str(inter_list[0]) + ' ' + str(inter_list[1]) + ' ' + str(inter_list[2]) + ' ' + str(inter_list[3]) + '\n')
	'''
	print (path)
	cv2.imshow("Before NMS", orig)
	cv2.imshow("After NMS", image)
	cv2.waitKey(0)