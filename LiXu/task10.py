import numpy as np
from collections import Counter
import glob as gb
import json
import sys

def comparator(l1, l2):
    if l1[0] != l2[0]:
        return l1[0] - l2[0]
    if l1[1] != l2[1]:
        return l1[1] - l2[1]
    return l1[2] - l2[2]

def readall(fname):
    with open(fname) as f:
        all_lines = f.readlines()
    all_lines = [x.strip().split(' ') for x in all_lines]
    result=[]
    for line in all_lines:
        result.append([int(x.strip(',')) for x in line])
    result.sort()
    return result

def findmax(r, g, b):
    file_path = gb.glob('output/cbody_nobody_frame*.txt')
    temp_body = []
    temp_nobody = []
    for path in file_path:
        cbody_nobody_frames = readall(path)
        #max_body = cbody_nobody_frames[0][3]
        #max_nobody = cbody_nobody_frames[0][4]
        for i in range(len(cbody_nobody_frames)):
            #print (i)
            flag = False
            v = comparator(cbody_nobody_frames[i], [r,g,b])
            if v == 0:
                #max_body = max(max_body, cbody_nobody_frames[i][3])
                #max_nobody = max(max_nobody, cbody_nobody_frames[i][4])
                temp_body.append(cbody_nobody_frames[i][3])
                temp_nobody.append(cbody_nobody_frames[i][4])
                flag = True
            elif v < 0:
                continue
            else:
                #temp_body.append(0)
                #temp_nobody.append(0)
                break
            if flag == True:
                break
    #print (temp_body)
    #print (temp_nobody)
    return max(temp_body), max(temp_nobody)
    #return max_body, max_nobody


#print (findmax(0,0,4))

with open('cbody_nobody.txt', 'r') as file:
    cbody_nobody = readall('cbody_nobody.txt')
    with open('cbody_nobody_final.txt', 'a') as file1:
        for index in range(len(cbody_nobody)):
            currentline=cbody_nobody[index]
            max_cbody,max_nobody=findmax(currentline[0],currentline[1],currentline[2])
            file1.write(str(currentline[0]) + ' '+str(currentline[1]) + ' '+str(currentline[2])+ ' ' +str(currentline[3]) + ' '+str(currentline[4]) + ' '+str(max_cbody)+' '+str(max_nobody)+'\n')
            print (index)