import numpy as np
from scipy.stats import laplace

#print laplace.cdf(1)

def lap(x, loc, scale):
	return laplace.cdf(x, loc, scale)

'''for i in range(0,100000001):
	print lap(i, 1649, 8/2)'''
#print lap(1659, 1649, 8/2)

def lapnoise(xi, ci, threshold, epsilon_prime):
	x = xi
	while lap(x, xi, ci/(2*epsilon_prime)) < threshold:
		x += 1
	return int(np.floor(2*xi + np.random.laplace(0, ci/(2*epsilon_prime)) - (x - 1)))

#print (lapnoise(1649, 8, 0.95, 2))