import laplace

with open('frame1.txt', 'r') as file1:
	with open('laplace_text.txt', 'a') as file2:
		for line in file1:
			currentline = line.split(' ')
			number = int(currentline[3])
			file2.write(str(laplace.lapnoise(number,number,0.95,2)) + '\n')