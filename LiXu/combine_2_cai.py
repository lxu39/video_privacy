import cv2
import numpy as np
from collections import Counter
import glob as gb
import json
import sys

file_path = gb.glob('frame*[0-9].txt')
for path in file_path:
	print (path)
	#filename is 'frame*'
	filename = path.split('.')[0]
	with open(path, 'r') as file1:
		with open(filename + '_cbody.txt', 'r') as file2:
			with open('cbody_nobody_' + filename + '.txt', 'w') as file3:
				for line1 in file1:
					#print (line1)
					flag = False
					currentline1 = line1.split(' ')
					file2.seek(0)
					for line2 in file2:
						currentline2 = line2.split(' ')
						if currentline1[0] == currentline2[0] and currentline1[1] == currentline2[1] and currentline1[2] == currentline2[2]:
							file3.write(currentline1[0] + ' ' + currentline1[1] + ' ' + currentline1[2] + ' ' + currentline2[3].rstrip() +' ' + str(int(currentline1[3].rstrip())-int(currentline2[3].rstrip())) + '\n')
							flag = True
							break
					if flag == False:
						file3.write(currentline1[0] + ' ' + currentline1[1] + ' ' + currentline1[2] + ' ' + str(0) +' '+ currentline1[3].rstrip() + '\n')