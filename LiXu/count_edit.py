import cv2
import numpy as np
from collections import Counter
import glob as gb
import json
import sys

def edit(input, output):
	with open(input, 'r') as file1:
		with open(output, 'w') as file2:
			for line in file1:
				currentline = line.split(" (")
				for item in currentline:
					temp = str(item).lstrip('"Counter({(').rstrip('})"').split('):')
					try:
						result = temp[0].rstrip() + temp[1].rstrip(',')
						file2.write(result+'\n')
					except:
						pass