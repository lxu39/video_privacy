import numpy as np
from collections import Counter
import glob as gb
import json
import sys

def comparator(l1, l2):
    if l1[0] != l2[0]:
        return l1[0] - l2[0]
    if l1[1] != l2[1]:
        return l1[1] - l2[1]
    return l1[2] - l2[2]

def readall(fname):
    with open(fname) as f:
        all_lines = f.readlines()
    all_lines = [x.strip().split(' ') for x in all_lines]
    result=[]
    for line in all_lines:
        result.append([int(x.strip(',')) for x in line])
    result.sort()
    return result


frames = readall('split_remove10_sorted.txt')
frame_cbodys = readall('cbody.txt')

frames_idx = 0
frame_cbodys_idx = 0

last_frames=[]
last_frame_cbodys=[]

with open('cbody_nobody.txt', 'w') as output:
    while True:
        if frames_idx >= len(frames):
            break
        currentline1 = frames[frames_idx]
        if len(last_frames) != 0:
            assert comparator(last_frames, currentline1) <= 0, "err"
        last_frames = currentline1
        if frame_cbodys_idx >= len(frame_cbodys):
            output.write(str(currentline1[0]) + ', ' + str(currentline1[1]) + ', ' + str(currentline1[2]) + ' ' + str(0) +' '+ str(currentline1[3]) + '\n')
            frames_idx+=1
        else:
            currentline2 = frame_cbodys[frame_cbodys_idx]
            if len(last_frame_cbodys) != 0:
                assert comparator(last_frame_cbodys, currentline2) <= 0, "err"
            last_frame_cbodys = currentline2
            v = comparator(currentline1, currentline2)
            if v < 0:
                frames_idx+=1
                output.write(str(currentline1[0]) + ', ' + str(currentline1[1]) + ', ' + str(currentline1[2]) + ' ' + str(0) +' '+ str(currentline1[3]) + '\n')
            elif v > 0:
                frame_cbodys_idx+=1
            else:
                output.write(str(currentline1[0]) + ', ' + str(currentline1[1]) + ', ' + str(currentline1[2]) + ' ' + str(currentline2[3]) + ' ' + str(currentline1[3] - currentline2[3]) + '\n')
                frames_idx+=1
                frame_cbodys_idx+=1
