def comparator(l1, l2):
    if l1[0] != l2[0]:
        return l1[0] - l2[0]
    if l1[1] != l2[1]:
        return l1[1] - l2[1]
    return l1[2] - l2[2]

def readall(fname):
    with open(fname) as f:
        all_lines = f.readlines()
    all_lines = [x.strip().split(' ') for x in all_lines]
    result=[]
    for line in all_lines:
        result.append([int(x.strip(',')) for x in line])
    result.sort()
    return result

with open('cbody_sorted.txt', 'a') as file1:
		lists = readall('cbody.txt')
		for item in lists:
			file1.write(str(item[0]) + ', ' + str(item[1]) + ', ' + str(item[2]) + ' ' + str(item[3]) + '\n')