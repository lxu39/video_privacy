import cv2
import numpy as np
from collections import Counter
import glob as gb
import json
import sys

file_path = gb.glob('output/cbody_nobody_frame*.txt')
for path in file_path:
	filename = path.split('.')[0].split('_')[2]
	print (path)
	with open('cbody_nobody.txt', 'r') as file1:
		with open(path, 'r') as file2:
			with open('output_9/' + filename + '_final.txt', 'w') as file3:
				for line1 in file1:
					flag = False
					currentline1 = line1.split(' ')
					file2.seek(0)
					for line2 in file2:
						currentline2 = line2.split(' ')
						if currentline1[0] == currentline2[0] and currentline1[1] == currentline2[1] and currentline1[2] == currentline2[2]:
							file3.write(currentline1[0] + ' ' + currentline1[1] + ' ' + currentline1[2] + ' ' + currentline1[3] + ' ' + currentline1[4].rstrip() + ' ' + currentline2[3] + ' ' + currentline2[4].rstrip() + '\n')
							flag = True
							break
					if flag == False:
						file3.write(currentline1[0] + ' ' + currentline1[1] + ' ' + currentline1[2] + ' ' + currentline1[3] + ' ' + currentline1[4].rstrip() + ' ' + str(0) + ' ' + str(0) + '\n')