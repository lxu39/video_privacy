import cv2
import glob as gb

# Create the haar cascade
faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
# Read the image
img_path = gb.glob('frame*.jpg')
for path in img_path:
	image = cv2.imread(path)
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	# Detect faces in the image
	faces = faceCascade.detectMultiScale(
	    gray,
	    scaleFactor=1.1,
	    minNeighbors=5,
	    minSize=(30, 30),
	    flags = cv2.CASCADE_SCALE_IMAGE
	)
	'''print (path)
	print ("Found {0} faces!".format(len(faces)))'''
	if len(faces) != 1:
		print (path)

	# Draw a rectangle around the faces
	for (x, y, w, h) in faces:
	    cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)
	    with open('face_rectangle.txt', 'a') as file:
	    	file.write(path +' ' + str(x) +' ' + str(y) +' '+ str(x+w) +' '+ str(y+h) + '\n')

	'''cv2.imshow("Faces found" ,image)
	cv2.waitKey(0)'''