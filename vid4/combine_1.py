import cv2
import numpy as np
from collections import Counter
import glob as gb
import json
import sys

def combine_1(input_1,input_2, output):
	with open(input_1, 'r') as file1:
		with open(input_2, 'r') as file2:
			with open(output, 'a') as file3:
				for line1 in file1:
					print (line1)
					flag = False
					currentline1 = line1.split(' ')
					file2.seek(0)
					for line2 in file2:
						currentline2 = line2.split(' ')
						if currentline1[0] == currentline2[0] and currentline1[1] == currentline2[1] and currentline1[2] == currentline2[2]:
							file3.write(currentline1[0] + ' ' + currentline1[1] + ' ' + currentline1[2] + ' ' + currentline2[3].rstrip() +' ' + str(int(currentline1[3].rstrip())-int(currentline2[3].rstrip())) + '\n')
							flag = True
							break
					if flag == False:
						file3.write(currentline1[0] + ' ' + currentline1[1] + ' ' + currentline1[2] + ' ' + str(0) +' '+ currentline1[3].rstrip() + '\n')
				

combine_1('split_remove10_sorted.txt', 'cface_sorted.txt', 'cface_noface.txt')